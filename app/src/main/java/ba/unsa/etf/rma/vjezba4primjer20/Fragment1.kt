package ba.unsa.etf.rma.vjezba4primjer20

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class Fragment1 : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment1_layout, container, false)

        var button : Button = view.findViewById(R.id.button)
        button.setOnClickListener {
            var fragment2 = Fragment2()
            openFragment(fragment2)
        }



        return view
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = parentFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, "page2")
        transaction.addToBackStack("page2")
        transaction.commit()
    }
}