package ba.unsa.etf.rma.vjezba4primjer20

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var fragment1 = Fragment1()
        openFragment(fragment1, "page1")
    }

    private fun openFragment(fragment: Fragment, tag : String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commit()
    }

    override fun onBackPressed() {
        val fragmentTag = supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1).name
        if(fragmentTag == "page1") {
            finish()
        }
        else if(fragmentTag == "page2") {
            openFragment(Fragment3(), "page3")
        }
        else if(fragmentTag == "page3") {
            openFragment(Fragment1(), "page1")
        }
    }
}