package ba.unsa.etf.rma.vjezba4primjer20

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class Fragment2 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment2_layout, container, false)

        var button : Button = view.findViewById(R.id.button2)
        button.setOnClickListener {
            var fragment3 = Fragment3()
            openFragment(fragment3)
        }
        return view
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = parentFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, "page3")
        transaction.addToBackStack("page3")
        transaction.commit()
    }
}