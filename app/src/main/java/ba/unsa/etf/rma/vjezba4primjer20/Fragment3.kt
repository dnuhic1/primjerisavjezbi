package ba.unsa.etf.rma.vjezba4primjer20

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class Fragment3 : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment3_layout, container, false)

        var button : Button = view.findViewById(R.id.button3)
        button.setOnClickListener {
            var fragment1 = Fragment1()
            openFragment(fragment1)
        }



        return view
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = parentFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, "page1")
        transaction.addToBackStack("page1")
        transaction.commit()
    }
}